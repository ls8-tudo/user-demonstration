# README #

This is a short demonstration of how to create an user within a container with a given uid and gid. The username / uid / gid and groupname have to be adjusted in the ENV of the Dockerfile. 
These parameters can be determined by calling the following command on the cluster home machine:
```
id
```

### How to build the container ###

To build the image run:
```
docker build -t ${REGISTRY}:${REGISTRY_PORT}/${USERNAME}/${IMAGENAME}:${VERSION}
```
Afterwards push the image to the local registry:
```
docker push ${REGISTRY}:${REGISTRY_PORT}/${USERNAME}/${IMAGENAME}:${VERSION}
```
Run the image with your mounted home directory:
```
docker run -it --rm --name ${USER}-${NAME} -c 1 -m 1g -v /home/${USER}:/home/${USER} ${REGISTRY}:${REGISTRY_PORT}/${USERNAME}/${IMAGENAME}:${VERSION} /bin/bash
```
Afterwards you can access the data from your home directory within the container. 